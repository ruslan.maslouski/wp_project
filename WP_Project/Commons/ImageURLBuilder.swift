//
//  ImageURLBuilder.swift
//  WP_Project
//
//  Created by Ruslan Maslouski on 22.09.21.
//

import Foundation
import UIKit

extension URL {
    init?(quizeImage: String, width: CGFloat, height: CGFloat) {
        let url = quizeImage.replacingOccurrences(of: "https://", with: "")
        self.init(string: "http://i.wpimg.pl/\(Int(width))x\(Int(height))/\(url)")
    }
}
