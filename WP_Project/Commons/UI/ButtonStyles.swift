//
//  QuizAnswerButtonStyle.swift
//  WP_Project
//
//  Created by Ruslan Maslouski on 23.09.21.
//

import SwiftUI

struct QuizAnswerButtonStyle: ButtonStyle {

    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .frame(minWidth: 0, maxWidth: .infinity)
            .padding()
            .foregroundColor(.white)
            .background(Color.black.opacity(0.5))
            .cornerRadius(40)
            .overlay(
                RoundedRectangle(cornerRadius: 40)
                    .stroke(Color.black, lineWidth: 2)
            )
            .scaleEffect(configuration.isPressed ? 0.96: 1)
    }
}

struct RectangleButtonStyle: ButtonStyle {

    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .frame(minWidth: 0, maxWidth: .infinity)
            .padding()
            .foregroundColor(.white)
            .background(Color.black.opacity(0.5))
            .border(Color.black, width: 2)
            .scaleEffect(configuration.isPressed ? 0.96: 1)
    }
}

struct GradientBackgroundStyle_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            Button("QuizAnswerButtonStyle") {}
                .buttonStyle(QuizAnswerButtonStyle())
            Button("RectangleButtonStyle") {}
                .buttonStyle(RectangleButtonStyle())
        }
    }
}
