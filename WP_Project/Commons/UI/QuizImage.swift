//
//  QuizImage.swift
//  WP_Project
//
//  Created by Ruslan Maslouski on 27.09.21.
//

import SwiftUI
import Kingfisher

struct QuizImage: View {

    let url: String?

    @State private var isFailed: Bool = false

    var body: some View {

        guard !isFailed else {
            return Image("ImagePlaceholder")
                .resizable(resizingMode: .stretch)
                .eraseToAnyView()
        }

        return GeometryReader { (geometry) in
            KFImage(url.flatMap { URL(quizeImage: $0, width: geometry.size.width, height: geometry.size.height) })
                .placeholder({ progress in
                    VStack(alignment: .center) {
                        ProgressView(progress)
                    }
                })
                .onFailure({ _ in
                    isFailed = true
                })
                .resizable()
                .aspectRatio(1, contentMode: .fill)
        }
        .eraseToAnyView()
    }
}

struct QuizImage_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            Text("===")
            QuizImage(url: nil)
                .scaledToFill()
//                .aspectRatio(1, contentMode: .fill)
                .frame(height: 200)
                .clipped()
                .background(Color.yellow)

            Text("===")
        }
    }
}
