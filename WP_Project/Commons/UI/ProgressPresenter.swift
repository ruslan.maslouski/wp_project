//
//  ActivityIndicatorViewPresenter.swift
//
//  Created by Ruslan Maslouski on 01/03/2021.
//

import SwiftUI

struct ActivityIndicatorViewPresenter<Content: View>: View {

    let userInteractionOnHUD: Bool
    let presenting: Bool
    private let content: () -> Content

    public init(userInteractionOnHUD: Bool = false, presenting: Bool, @ViewBuilder content: @escaping () -> Content) {
        self.userInteractionOnHUD = userInteractionOnHUD
        self.content = content
        self.presenting = presenting
    }

    public var body: some View {
        ZStack {
            content()

            if presenting {
                ProgressView()
                    .scaleEffect(x: 1.5, y: 1.5, anchor: .center)
                    .padding(30)
                    .background(Color.black.opacity(0.7))
                    .cornerRadius(15)
                    .progressViewStyle(CircularProgressViewStyle(tint: Color.white))             .edgesIgnoringSafeArea(.all)
            }
        }
        .allowsHitTesting(userInteractionOnHUD || !presenting)
    }
}

struct ContentView: View {
    @State var isLoading = false

    var body: some View {
        ActivityIndicatorViewPresenter(userInteractionOnHUD: true, presenting: isLoading) {
            VStack {
                Text("Item 1")
                    .foregroundColor(isLoading ? .red : .green)
                Text("Item 2")
                Text("Item 3")
                Text("Item 4")
                Button("Dismiss") {
                    isLoading = false
                }
                Toggle(isOn: $isLoading) {
                    Text("First toggle")
                }
            }
        }
    }
}

struct ProgressPresenter_Previews: PreviewProvider {

    static var previews: some View {
        ContentView()
    }
}
