//
//  DefaultsKeys.swift
//  WP_Project
//
//  Created by Ruslan Maslouski on 24.09.21.
//

import Foundation
import SwiftyUserDefaults

extension DefaultsKeys {
    var quizHistory: DefaultsKey<[QuizHistory]> { .init("QuizHistoryServiceKey", defaultValue: []) }
    var quizRecovery: DefaultsKey<[QuizRecovery]> { .init("QuizRecoveryServiceKey", defaultValue: []) }
}
