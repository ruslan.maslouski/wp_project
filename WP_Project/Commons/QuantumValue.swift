//
//  QuantumValue.swift
//  WP_Project
//
//  Created by Ruslan Maslouski on 23.09.21.
//

import Foundation

enum QuantumValue: Decodable {

    case int(Int), string(String)

    init(from decoder: Decoder) throws {
        if let int = try? decoder.singleValueContainer().decode(Int.self) {
            self = .int(int)
            return
        }

        if let string = try? decoder.singleValueContainer().decode(String.self) {
            self = .string(string)
            return
        }

        throw QuantumError.missingValue
    }

    enum QuantumError: Error {
        case missingValue
    }

}

extension QuantumValue {
    var stringValue: String {
        switch self {
        case .int(let value):
            return "\(value)"
        case .string(let value):
            return value
        }
    }

    var intValue: Int? {
        switch self {
        case .int(let value):
            return value
        case .string(let value):
            return Int(value)
        }
    }
}
