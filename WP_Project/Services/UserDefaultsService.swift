//
//  UserDefaultsService.swift
//  WP_Project
//
//  Created by Ruslan Maslouski on 25.09.21.
//

import SwiftyUserDefaults
import Combine

class UserDefaultsService<Value: DefaultsSerializable> where Array<Value> == Array<Value>.Bridge.T  {

    private let key: KeyPath<DefaultsKeys, DefaultsKey<[Value]>>

    let didChangePassthroughSubject: CurrentValueSubject<[Value]?, Never>

    private var dispose: DefaultsDisposable?
    init(key: KeyPath<DefaultsKeys, DefaultsKey<[Value]>>){
        self.key = key

        let currentQuizHistory = Defaults[key]
        self.didChangePassthroughSubject = CurrentValueSubject(currentQuizHistory)

        dispose = Defaults.observe(key) { [weak self] update in
            let value = update.newValue
            self?.didChangePassthroughSubject.send(value)
        }
    }

    func addItem(_ item: Value) {
        Defaults[key].append(item)
    }

}
