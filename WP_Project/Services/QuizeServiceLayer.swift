//
//  QuizeServiceLayer.swift
//  WP_Project
//
//  Created by Ruslan Maslouski on 22.09.21.
//

import Foundation
import Combine

protocol QuizeServiceLayerType {
    func getQuizzes() -> AnyPublisher<[ShortQuiz], Error>
    func getQuiz(id: Int) -> AnyPublisher<FullQuiz, Error>
}

class QuizeServiceLayer: QuizeServiceLayerType {

    func getQuizzes() -> AnyPublisher<[ShortQuiz], Error> {
        let url = URL(string: "http://quiz.o2.pl/api/v1/quizzes/0/100")!
        return ServiceLayer().request(url: url, httpMethod: .GET).map { (wrapper: QuizeWrapper) in
            return wrapper.items
        }.eraseToAnyPublisher()
    }

    func getQuiz(id: Int) -> AnyPublisher<FullQuiz, Error> {
        let url = URL(string: "http://quiz.o2.pl/api/v1/quiz/\(id)/0")!
        return ServiceLayer().request(url: url, httpMethod: .GET)
    }

}

private class QuizeWrapper: Decodable {
    var items: [ShortQuiz]
}
