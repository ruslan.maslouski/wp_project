//
//  QuizRecoveryService.swift
//  WP_Project
//
//  Created by Ruslan Maslouski on 25.09.21.
//

import SwiftyUserDefaults
import Combine

protocol QuizRecoveryService {
    var didChangePassthroughSubject: CurrentValueSubject<[QuizRecovery]?, Never> { get }
    func addItem(_ item: QuizRecovery)
}

extension UserDefaultsService: QuizRecoveryService where Value == QuizRecovery {
}
