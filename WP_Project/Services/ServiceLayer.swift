//
//  ServiceLayer.swift
//  WP_Project
//
//  Created by Ruslan Maslouski on 22.09.21.
//

import Foundation
import Combine

class ServiceLayer {

    enum Method: String {
        case GET = "GET"
    }

    func request<T: Decodable>(url: URL, httpMethod: Method) -> AnyPublisher<T, Error> {
        var request = URLRequest(url: url)

        request.httpMethod = httpMethod.rawValue

        return URLSession.shared.dataTaskPublisher(for: request)
            .map(\.data)
            .map({ data in

//                print("\( String(data: data, encoding: .utf8) )")

                let json = try! JSONSerialization.jsonObject(with: data, options: [])
                print("\(json)")

                return data
            })
            .decode(type: T.self, decoder: JSONDecoder())
            .eraseToAnyPublisher()
    }

}
