//
//  QuizHistoryService.swift
//  WP_Project
//
//  Created by Ruslan Maslouski on 24.09.21.
//

import Foundation
import SwiftyUserDefaults
import Combine

// not for test app* use CoreData
//I think not make sence to SetUp coredata stack here, as for good needs to do a smart cache for all quiz with change policy (add, update, delete), relationship to QuizHistory and for not finished Quizzes

protocol QuizHistoryService {
    var didChangePassthroughSubject: CurrentValueSubject<[QuizHistory]?, Never> { get }
    func addItem(_ item: QuizHistory)
}

extension UserDefaultsService: QuizHistoryService where Value == QuizHistory {
}
