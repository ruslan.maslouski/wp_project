//
//  ShortQuiz.swift
//  WP_Project
//
//  Created by Ruslan Maslouski on 22.09.21.
//

import Foundation

struct ShortQuiz: Decodable {
    var id: Int
    var questions: Int
    var title: String
    var mainPhoto: MainPhoto
}
