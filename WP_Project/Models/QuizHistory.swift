//
//  QuizHistory.swift
//  WP_Project
//
//  Created by Ruslan Maslouski on 24.09.21.
//

import Foundation
import SwiftyUserDefaults

struct QuizHistory: Codable, DefaultsSerializable {
    let quizID: String
    let date: Date
    let questions: [Bool]
    let result: Double
}
