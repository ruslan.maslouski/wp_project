//
//  FullQuiz.swift
//  WP_Project
//
//  Created by Ruslan Maslouski on 23.09.21.
//

import Foundation

struct FullQuiz: Decodable {

    struct Rate: Decodable {
        var from: Int
        var to: Int
        var content: String
    }

    struct Question: Decodable {
        var text: String
        var image: MainPhoto
        var answers: [Answer]
    }

    struct Answer: Decodable {
        var text: String?
        var order: Int
        var isCorrect: Bool

        enum CodingKeys: String, CodingKey {
            case text
            case order
            case isCorrect
        }

        init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            let textValue = try? container.decode(QuantumValue.self, forKey: .text)
            let isCorrectValue = (try? container.decode(Int.self, forKey: .isCorrect)) ?? 0

            text = textValue?.stringValue
            order = try container.decode(Int.self, forKey: .order)
            isCorrect = isCorrectValue == 1
        }
    }

    var id: Int
    var title: String
    var rates: [Rate]
    var questions: [Question]
    var mainPhoto: MainPhoto
}
