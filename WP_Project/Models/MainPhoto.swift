//
//  MainPhoto.swift
//  WP_Project
//
//  Created by Ruslan Maslouski on 23.09.21.
//

import Foundation

struct MainPhoto: Decodable {
    var url: String
    var width: Int
    var height: Int

    enum CodingKeys: String, CodingKey {
        case url
        case width
        case height
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let widthValue = try? container.decode(QuantumValue.self, forKey: .width).intValue
        let heightValue = try? container.decode(QuantumValue.self, forKey: .height).intValue

        url = try container.decode(String.self, forKey: .url)
        width = widthValue ?? 0
        height = heightValue ?? 0
    }

}
