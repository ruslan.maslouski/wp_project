//
//  QuizRecovery.swift
//  WP_Project
//
//  Created by Ruslan Maslouski on 25.09.21.
//

import Foundation
import SwiftyUserDefaults

struct QuizRecovery: Codable, DefaultsSerializable {
    let quizID: String
    let questions: [Bool]
}
