//
//  UIApplication+dependencyContainer.swift
//  WP_Project
//
//  Created by Ruslan Maslouski on 25.09.21.
//

import UIKit
import Swinject

protocol DependencyContainerHolding {
    var container: Container { get }
}

extension UIApplication {
    static var container: Container {
        guard let container = (UIApplication.shared.delegate as? DependencyContainerHolding)?.container else {
            fatalError("UIApplication delegate needs to implement DependencyContainerHolding to provide a Container")
        }
        return container
    }
}
