//
//  QuizRowView.swift
//  WP_Project
//
//  Created by Ruslan Maslouski on 22.09.21.
//

import SwiftUI
import Kingfisher

protocol QuizeRowViewModeling {
    var title: String { get }
    var image: String? { get }
    var score: Double? { get }
}

struct QuizRowView: View {
    let item: QuizeRowViewModeling
    var body: some View {
        ZStack {
            QuizImage(url: item.image)
                .scaledToFill()
                .frame(height: 200)
                .clipped()

            VStack() {
                Text(item.title)
                    .foregroundColor(.white)
                    .font(.headline)
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding(8)
                    .background(
                            LinearGradient(gradient: Gradient(colors: [.black, .clear]), startPoint: .top, endPoint: .bottom)
                        )

                Spacer()

                if let score = item.score {
                    Text("Last score \(Int(score * 100))")
                        .foregroundColor(.white)
                        .frame(maxWidth: .infinity, alignment: .center)
                        .padding(8)
                        .background(
                                LinearGradient(gradient: Gradient(colors: [.clear, .black]), startPoint: .top, endPoint: .bottom)
                            )
                }
            }
        }
        .clipped()
    }
}
