//
//  QuizzesViewModel.swift
//  WP_Project
//
//  Created by Ruslan Maslouski on 22.09.21.
//

import UIKit
import Combine

enum QuizzesViewModelResult {
    case showQuiz(_ result: FullQuiz)
}

class QuizzesViewModel: QuizzesViewModeling {
    
    let result: PassthroughSubject<QuizzesViewModelResult, Never>

    private let quizHistoryService: QuizHistoryService

    @Published private var quizzes = [ShortQuiz]()
    @Published var isLoading: Bool = false

    var numberOfItems: Int {
        quizzes.count
    }

    private let fullQuizAction: Action<Int, FullQuiz, Error>
    private let listOfQuizAction: Action<Void, [ShortQuiz], Error>

    private var subscriptions = Set<AnyCancellable>()
    // not for test app* create dependancy injection for QuizHistoryService
    init(quizeServiceLayer: QuizeServiceLayerType = QuizeServiceLayer(), quizHistoryService: QuizHistoryService = UIApplication.container.resolve()) {
        self.quizHistoryService = quizHistoryService
        self.result = PassthroughSubject()

        fullQuizAction = Action(execute: { id in
            quizeServiceLayer.getQuiz(id: id)
        })

        listOfQuizAction = Action(execute: { _ in
            quizeServiceLayer.getQuizzes()
        })

        listOfQuizAction.apply()
            .replaceError(with: [])
            .receive(on: DispatchQueue.main)
            .assign(to: &$quizzes)

        fullQuizAction.isExecutingPublisher
            .combineLatest(listOfQuizAction.isExecutingPublisher)
            .map { $0 || $1 }
            .receive(on: DispatchQueue.main)
            .assign(to: &$isLoading)

        quizHistoryService.didChangePassthroughSubject.sink { [weak self] _ in
            self?.objectWillChange.send()
        }.store(in: &subscriptions)
    }

    func rowViewModel(at index: Int) -> QuizeRowViewModeling {
        let quiz = quizzes[index]
        let history = quizHistoryService.didChangePassthroughSubject.value
        let last = history?.last(where: { $0.quizID == "\(quiz.id)" })
        return QuizeRowViewModel(quiz: quiz, score: last?.result)
    }

    func didSelectQuiz(_ index: Int) {
        let quiz = quizzes[index]

        fullQuizAction.apply(quiz.id)
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { completion in
                print("receiveCompletion \(completion)")
            }, receiveValue: { [weak self] fullQuiz in
                self?.result.send(.showQuiz(fullQuiz))
            })
            .store(in: &subscriptions)
    }

}

class QuizeRowViewModel: QuizeRowViewModeling {
    let id = UUID()
    private let quiz: ShortQuiz

    var title: String {
        return quiz.title
    }

    var image: String? {
        return quiz.mainPhoto.url
    }

    var score: Double?

    init(quiz: ShortQuiz, score: Double?) {
        self.quiz = quiz
        self.score = score
    }
}
