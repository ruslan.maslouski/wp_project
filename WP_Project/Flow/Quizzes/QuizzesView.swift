//
//  QuizzesView.swift
//  WP_Project
//
//  Created by Ruslan Maslouski on 22.09.21.
//

import SwiftUI

protocol QuizzesViewModeling: ObservableObject {
    var isLoading: Bool { get }
    var numberOfItems: Int { get }

    func rowViewModel(at index: Int) -> QuizeRowViewModeling
    func didSelectQuiz(_ index: Int)
}

struct QuizzesView<ViewModel>: View where ViewModel: QuizzesViewModeling {

    @Environment(\.presentationMode) var presentation
    @ObservedObject var viewModel: ViewModel

    var body: some View {
        ActivityIndicatorViewPresenter(userInteractionOnHUD: false, presenting: viewModel.isLoading) {
            List() {
                ForEach(0..<viewModel.numberOfItems, id: \.self) { index in
                    Button(action: {
                        viewModel.didSelectQuiz(index)
                    }, label: {
                        QuizRowView(item: viewModel.rowViewModel(at: index))
                    })
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        QuizzesView(viewModel: MockQuizzesViewModel())
    }
}

class MockQuizzesViewModel: QuizzesViewModeling {

    var isLoading: Bool { false }

    var numberOfItems: Int {
        return 4
    }

    func rowViewModel(at index: Int) -> QuizeRowViewModeling {
        MockQuizeRowViewModel(title: "title", image: nil, score: 0.64)
    }

    func didSelectQuiz(_ index: Int) {
    }

}

struct MockQuizeRowViewModel: QuizeRowViewModeling {
    var title: String
    var image: String?
    var score: Double?
}
