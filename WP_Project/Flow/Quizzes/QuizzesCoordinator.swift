//
//  QuizzesCoordinator.swift
//  WP_Project
//
//  Created by Ruslan Maslouski on 23.09.21.
//

import UIKit
import SwiftUI

class QuizzesCoordinator {
    let window: UIWindow
    let rootViewController: UINavigationController?

    init(window: UIWindow) {
        self.window = window
        rootViewController = UINavigationController()
    }

    func start() {
        let vm = QuizzesViewModel()
        let contentView = QuizzesView(viewModel: vm)
        let hostingViewController = UIHostingController(rootView: contentView)

        rootViewController?.pushViewController(hostingViewController, animated: false)

        window.rootViewController = rootViewController
        window.makeKeyAndVisible()

        vm.result.sinkForLifetimeOf(hostingViewController, receiveValue: { [weak self] result in
            guard case .showQuiz(let quiz) = result else {
                return
            }
            self?.presentQuiz(quiz)
        })
    }

    private func presentQuiz(_ quiz: FullQuiz) {
        guard let rootViewController = rootViewController else {
            return
        }
        let coordinator = QuizCoordinator(presenter: rootViewController)
        coordinator.start(quiz: quiz)
    }

}
