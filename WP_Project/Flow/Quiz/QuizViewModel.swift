//
//  QuizViewModel.swift
//  WP_Project
//
//  Created by Ruslan Maslouski on 23.09.21.
//

import Foundation
import Combine
import UIKit

enum QuizViewModelResult {
    case showResult(_ result: QuizResult)
}

class QuizViewModel: QuizViewModeling {

    let result: PassthroughSubject<QuizViewModelResult, Never>

    private let quizHistoryService: QuizHistoryService

    private let quiz: FullQuiz

    private var currentQuestionIndex: Int {
        didSet {
            objectWillChange.send()
        }
    }
    private var history: [Bool]

    var title: String {
        quiz.title
    }

    var progressAmount: Double {
        return Double(currentQuestionIndex) / Double(quiz.questions.count)
    }

    var image: String? {
        let currentQuestionImage = quiz.questions[currentQuestionIndex].image
        let image = !currentQuestionImage.url.isEmpty ? currentQuestionImage : quiz.mainPhoto
        return image.url
    }

    var question: String {
        quiz.questions[currentQuestionIndex].text
    }

    var answers: [String] {
        quiz.questions[currentQuestionIndex].answers.map { $0.text ?? "" }
    }

    // not for test app* create dependancy injection for QuizHistoryService
    init(quiz: FullQuiz, quizHistoryService: QuizHistoryService = UIApplication.container.resolve()) {
        self.quiz = quiz
        self.quizHistoryService = quizHistoryService

        self.history = []
        self.result = PassthroughSubject<QuizViewModelResult, Never>()

        currentQuestionIndex = 0
    }

    func tryAgain() {
        history = []
        currentQuestionIndex = 0
    }

    func answer(_ index: Int) {
        validateAnswer(index)

        if currentQuestionIndex == quiz.questions.count - 1 {
            quizFinished()
        } else {
            currentQuestionIndex += 1
        }
    }

    private func validateAnswer(_ index: Int) {
        let currentQuestion = quiz.questions[currentQuestionIndex]
        let isCorrect = currentQuestion.answers[index].isCorrect

        history.append(isCorrect)
    }

    private func quizFinished() {
        let rightAnswers = history.reduce(0, { $0 + ($1 ? 1 : 0) })
        let score = Double(rightAnswers) / Double(quiz.questions.count)
        let result = QuizResult(score: score)

        storeResult(history: history, score: score)

        self.result.send(.showResult(result))
    }

    private func storeResult(history: [Bool], score: Double) {
        let quizHistory = QuizHistory(quizID: "\(quiz.id)", date: Date(), questions: history, result: score)
        quizHistoryService.addItem(quizHistory)
    }
}
