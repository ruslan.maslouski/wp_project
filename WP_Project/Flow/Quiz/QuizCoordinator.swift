//
//  QuizCoordinator.swift
//  WP_Project
//
//  Created by Ruslan Maslouski on 23.09.21.
//

import Foundation
import SwiftUI
import Combine

class QuizCoordinator {
    private let presenter: UINavigationController
    private weak var quizHostingController: UIViewController?

    init(presenter: UINavigationController) {
        self.presenter = presenter
    }

    func start(quiz: FullQuiz) {
        let vm = QuizViewModel(quiz: quiz)
        let contentView = QuizView(viewModel: vm)
        let hostingViewController = UIHostingController(rootView: contentView)
        self.quizHostingController = hostingViewController

        presenter.present(hostingViewController, animated: true, completion: nil)

        vm.result.flatMap { result -> AnyPublisher<QuizResultViewModelResult, Never> in
            guard case .showResult(let result) = result else {
                return Empty().eraseToAnyPublisher()
            }
            return self.showResult(result: result)
        }.sinkForLifetimeOf(hostingViewController, receiveValue: { [weak presenter, weak hostingViewController, weak vm] result in
            switch result {
            case .closeQuiz:
                presenter?.dismiss(animated: true, completion: nil)
            case .tryAgain:
                vm?.tryAgain()
                hostingViewController?.dismiss(animated: true, completion: nil)
            }
        })
    }

    private func showResult(result: QuizResult) -> AnyPublisher<QuizResultViewModelResult, Never> {
        guard let quizHostingController = quizHostingController else {
            return Empty().eraseToAnyPublisher()
        }

        let coordinator = QuizResultCoordinator(presenter: quizHostingController)
        return coordinator.start(result: result)
    }
}
