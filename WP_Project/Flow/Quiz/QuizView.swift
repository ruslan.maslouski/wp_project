//
//  QuizView.swift
//  WP_Project
//
//  Created by Ruslan Maslouski on 22.09.21.
//

import SwiftUI
import Kingfisher

protocol QuizViewModeling: ObservableObject {
    var title: String { get }
    var progressAmount: Double { get }
    var image: String? { get }
    var question: String { get }
    var answers: [String] { get }

    func answer(_ index: Int)
}

struct QuizView<ViewModel>: View where ViewModel: QuizViewModeling {

    @ObservedObject var viewModel: ViewModel

    var body: some View {
        VStack(alignment: .center, spacing: 8) {
            Text(viewModel.title)
                .multilineTextAlignment(.center)
                .lineLimit(2)
            ProgressView(value: viewModel.progressAmount, total: 1)

            QuizImage(url: viewModel.image)
                .scaledToFill()
                .frame(height: 200)
                .clipped()

            Text(viewModel.question)
                .multilineTextAlignment(.center)
                .padding(.top, 30)
                .padding(.bottom, 10)

            Spacer()

            ForEach(Array(viewModel.answers.enumerated()), id: \.offset) { index, answer in
                Button(answer) {
                    viewModel.answer(index)
                }
                .buttonStyle(QuizAnswerButtonStyle())
            }.frame(maxWidth: .infinity, alignment: .leading)
        }
        .padding(8)
    }
}

struct QuizView_Previews: PreviewProvider {
    static var previews: some View {
        QuizView(viewModel: MockQuizViewModel())
    }
}

class MockQuizViewModel: QuizViewModeling {
    var title: String { "title title title title title title title title title title title title title title title title title title title title title title 1" }
    var progressAmount: Double { 0.5 }
    var image: String? { nil }
    var question: String { "question question question question question question question" }
    var answers: [String] { ["answer 1", "answer 2", "answer 3", "answer 4"] }

    func answer(_ index: Int) {

    }

}
