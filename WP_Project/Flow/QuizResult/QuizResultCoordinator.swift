//
//  QuizResultCoordinator.swift
//  WP_Project
//
//  Created by Ruslan Maslouski on 24.09.21.
//

import UIKit
import SwiftUI
import Combine

struct QuizResult {
    var score: Double
}

class QuizResultCoordinator {
    private let presenter: UIViewController
    private weak var quizHostingController: UIViewController?

    init(presenter: UIViewController) {
        self.presenter = presenter
    }

    func start(result: QuizResult) -> AnyPublisher<QuizResultViewModelResult, Never> {
        let vm = QuizResultViewModel(result: result)
        let contentView = QuizResultView(viewModel: vm)
        let hostingViewController = UIHostingController(rootView: contentView)
        hostingViewController.isModalInPresentation = true
        self.quizHostingController = hostingViewController

        presenter.present(hostingViewController, animated: true, completion: nil)

        return vm.result.eraseToAnyPublisher()
    }
}
