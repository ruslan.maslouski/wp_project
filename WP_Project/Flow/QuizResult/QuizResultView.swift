//
//  QuizResult.swift
//  WP_Project
//
//  Created by Ruslan Maslouski on 23.09.21.
//

import SwiftUI

protocol QuizResultViewModeling: ObservableObject {
    var score: Int { get }

    func tryAgain()
    func close()
}

struct QuizResultView<ViewModel>: View where ViewModel: QuizResultViewModeling {

    @ObservedObject var viewModel: ViewModel

    var body: some View {
        VStack(alignment: .center, spacing: 8) {
            Text("Brawo!")
                .font(.title)
            Spacer()
            Text("Twój wynik:")
            Text("\(viewModel.score)%")
                .font(.system(size: 56.0))

            Spacer()

            Button("Try again") {
                viewModel.tryAgain()
            }.buttonStyle(RectangleButtonStyle())

            Button("Return") {
                viewModel.close()
            }.buttonStyle(RectangleButtonStyle())

        }
        .padding(8)
    }
}

struct QuizResult_Previews: PreviewProvider {
    static var previews: some View {
        QuizResultView(viewModel: MockQuizResultViewModel())
    }
}

class MockQuizResultViewModel: QuizResultViewModeling {
    var score: Int { 54 }
    func tryAgain() { }
    func close() { }
}
