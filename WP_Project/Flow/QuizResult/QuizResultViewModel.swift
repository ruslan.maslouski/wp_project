//
//  QuizResultViewModel.swift
//  WP_Project
//
//  Created by Ruslan Maslouski on 23.09.21.
//

import Foundation
import Combine

enum QuizResultViewModelResult {
    case tryAgain
    case closeQuiz
}

class QuizResultViewModel: QuizResultViewModeling {

    let result: PassthroughSubject<QuizResultViewModelResult, Never>

    private let quizResult: QuizResult
    var score: Int {
        Int(quizResult.score * 100)
    }

    init(result: QuizResult) {
        self.quizResult = result
        self.result = PassthroughSubject<QuizResultViewModelResult, Never>()
    }

    func tryAgain() {
        result.send(.tryAgain)
    }

    func close() {
        result.send(.closeQuiz)
    }

}
