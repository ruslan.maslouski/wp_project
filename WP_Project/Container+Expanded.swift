//
//  Container.swift
//  WP_Project
//
//  Created by Ruslan Maslouski on 25.09.21.
//

import Swinject

extension Container {

    func resolve<T>() -> T {
        guard let result = resolve(T.self) else {
            preconditionFailure("WARNING: no service \(T.self) found")
        }
        return result
    }

}
